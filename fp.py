# encoding: utf-8
from __future__ import division
from collections import defaultdict, namedtuple
from itertools import combinations
from itertools import imap
from threading import Thread
import Queue
import xml.etree.ElementTree as ET
import timeit
import math

class FPTree(object):
    Route = namedtuple('Route', 'head tail')

    def __init__(self):
        # The root node of the tree.
        self._root = FPNode(self, None, None)

        # A dictionary mapping items to the head and tail of a path of
        # "neighbors" that will hit every node containing that item.
        self._routes = {}

    @property
    def root(self):
        """The root node of the tree."""
        return self._root

    def add(self, transaction):
        """Add a transaction to the tree."""
        point = self._root

        for item in transaction:
            next_point = point.search(item)
            if next_point:
                # There is already a node in this tree for the current
                # transaction item; reuse it.
                next_point.increment()
            else:
                # Create a new point and add it as a child of the point we're
                # currently looking at.
                next_point = FPNode(self, item)
                point.add(next_point)

                # Update the route of nodes that contain this item to include
                # our new node.
                self._update_route(next_point)

            point = next_point

    def _update_route(self, point):
        """Add the given node to the route through all nodes for its item."""
        assert self is point.tree

        try:
            route = self._routes[point.item]
            route[1].neighbor = point  # route[1] is the tail
            self._routes[point.item] = self.Route(route[0], point)
        except KeyError:
            # First node for this item; start a new route.
            self._routes[point.item] = self.Route(point, point)

    def items(self):
        for item in self._routes.iterkeys():
            yield (item, self.nodes(item))

    def nodes(self, item):

        try:
            node = self._routes[item][0]
        except KeyError:
            return

        while node:
            yield node
            node = node.neighbor

    def prefix_paths(self, item):
        def collect_path(node):
            path = []
            while node and not node.root:
                path.append(node)
                node = node.parent
            path.reverse()
            return path

        return (collect_path(node) for node in self.nodes(item))

    def inspect(self):
        roott = ET.Element('root')
        self.root.them(roott)
        ET.ElementTree(roott).write(
            'fptree.xml', encoding="UTF-8", xml_declaration=True)
        # print 'Tree:'
        # self.root.inspect(1)

        # print
        # print 'Routes:'
        # for item, nodes in self.items():
        # print '  %r' % item
        # for node in nodes:
        # print '    %r' % node


class FPNode(object):
    """A node in an FP tree."""

    def __init__(self, tree, item, count=1):
        self._tree = tree
        self._item = item
        self._count = count
        self._parent = None
        self._children = {}
        self._neighbor = None

    def add(self, child):
        """Add the given FPNode `child` as a child of this node."""

        if not isinstance(child, FPNode):
            raise TypeError("Can only add other FPNodes as children")

        if not child.item in self._children:
            self._children[child.item] = child
            child.parent = self

    def search(self, item):
        try:
            return self._children[item]
        except KeyError:
            return None

    def __contains__(self, item):
        return item in self._children

    @property
    def tree(self):
        """The tree in which this node appears."""
        return self._tree

    @property
    def item(self):
        """The item contained in this node."""
        return self._item

    @property
    def count(self):
        """The count associated with this node's item."""
        return self._count

    def increment(self):
        """Increment the count associated with this node's item."""
        if self._count is None:
            raise ValueError("Root nodes have no associated count.")
        self._count += 1

    @property
    def root(self):
        """True if this node is the root of a tree; false if otherwise."""
        return self._item is None and self._count is None

    @property
    def leaf(self):
        """True if this node is a leaf in the tree; false if otherwise."""
        return len(self._children) == 0

    @property
    def parent(self):
        """The node's parent"""
        return self._parent

    @parent.setter
    def parent(self, value):
        if value is not None and not isinstance(value, FPNode):
            raise TypeError("A node must have an FPNode as a parent.")
        if value and value.tree is not self.tree:
            raise ValueError("Cannot have a parent from another tree.")
        self._parent = value

    @property
    def neighbor(self):
        """
        The node's neighbor; the one with the same value that is "to the right"
        of it in the tree.
        """
        return self._neighbor

    @neighbor.setter
    def neighbor(self, value):
        if value is not None and not isinstance(value, FPNode):
            raise TypeError("A node must have an FPNode as a neighbor.")
        if value and value.tree is not self.tree:
            raise ValueError("Cannot have a neighbor from another tree.")
        self._neighbor = value

    @property
    def children(self):
        """The nodes that are children of this node."""
        return tuple(self._children.itervalues())

    def inspect(self, depth=0):
        print ('  ' * depth) + str(self._item) + ":" + str(self._count)
        for child in self.children:
            child.inspect(depth + 1)

    def them(self, root):
        for child in self.children:
            c = ET._Element(str(child._item))
            # c.attrib['name'] = str(child._item)
            c.attrib['count'] = str(child._count)
            root.append(c)
            child.them(c)

    def __repr__(self):
        if self.root:
            return "<%s (root)>" % type(self).__name__
        return "<%s %r (%r)>" % (type(self).__name__, self.item, self.count)


def find_frequent_itemsets(transactions, minimum_support):
    items = defaultdict(lambda: 0)  # Gan mat dinh tat ca item la 0

    # Doc tung transaction va tinh item count cho moi item
    for transaction in transactions:
        for item in transaction:
            items[item] += 1  # Cong vao 1 cho moi lan xuat hien cua item do

    # Xay tap items, khong bao gom cac item co count nho hon minimum_support
    items = dict((item, support) for item, support in items.iteritems()
                 if support >= minimum_support)

    #    print str(items)

    # Xoa di cac item  co < min_count, va sap xep giam dan theo count
    def clean_transaction(transaction):
        # loc lai lay cac item trong tap items da xay
        transaction = filter(lambda v: v in items, transaction)
        # sap xep giam dan
        transaction.sort(key=lambda v: items[v], reverse=True)
        return transaction

    # xay cay FPTree
    master = FPTree()
    for transaction in imap(clean_transaction, transactions):
        master.add(transaction)

    # xml gi gi do
    master.inspect()

    # suffix tien to check co cay
    def find_with_suffix(tree, suffix, queue):  # suffix tien to truoc cua cay

        # mang quan ly cac thread de chay va join lai
        threads = []

        # mang chua param cho ham chay find_with_suffix
        arrayItem = []
        for item, nodes in tree.items():
            arrayItem.append((item, nodes, tree, suffix, queue))
        for item in arrayItem:
            t = Thread(target=fws, args=item)
            threads.append(t)
        for x in threads:
            x.start()
        for x in threads:
            x.join()

    def fws(item, nodes, tree, suffix, queue):
        support = sum(n.count for n in nodes)
        if item not in suffix and support >= minimum_support:
            # Tim dc set moi
            found_set = [item] + suffix

            queue.put((found_set, support))
            # Build a conditional tree and recursively search for frequent
            # itemsets within it.
            cond_tree = conditional_tree_from_paths(tree.prefix_paths(item))

            find_with_suffix(cond_tree, found_set, queue)

    # tao queue de chua ket qua chung co tat ca cac thread
    queue = Queue.Queue()

    # chay tim kiem
    find_with_suffix(master, [], queue)

    # xuat ket qua tu queue
    while not queue.empty():
        yield (queue.get())


def conditional_tree_from_paths(paths):
    tree = FPTree()
    condition_item = None
    items = set()


    # Them cac nodes trong paths vo new tree. Chi lay count cua node leaf
    # Count cua cac node con lai se dc tinh tu duoi len
    for path in paths:
        if condition_item is None:
            condition_item = path[-1].item

        point = tree.root
        for node in path:
            next_point = point.search(node.item)
            if not next_point:
                # Add a new node to the tree.
                items.add(node.item)
                count = node.count if node.item == condition_item else 0
                next_point = FPNode(tree, node.item, count)
                point.add(next_point)
                tree._update_route(next_point)
            point = next_point

    assert condition_item is not None  # neu condition item rong thi throw exception
    # Tinh so node khong phai la leaf
    for path in tree.prefix_paths(condition_item):
        count = path[-1].count
        for node in reversed(path[:-1]):
            node._count += count

    return tree


if __name__ == '__main__':
    start = timeit.default_timer()
    from optparse import OptionParser
    import csv

    p = OptionParser(usage='%prog data_file')
    #-s tham so cho min sup count, mat dinh la 2
    p.add_option('-s', '--minimum-support', dest='minsup', type='int',
                 help='Minimum itemset support (default: 2)')
    p.add_option('-t', '--threshold', dest='threshold', type='float',
                 help='Threshold (default: 0.6)')
    # min_support_count mac dinh set la 2
    p.set_defaults(minsup=2)
    p.set_defaults(threshold=0.6)
    options, args = p.parse_args()
    if len(args) < 1:  # arg la duong dan file csv
        p.error('must provide the path to a CSV file to read')

    # tap cac transaction
    num_transaction = 0
    transactions = []
    with open(args[0]) as database:
        for row in csv.reader(database):
            num_transaction = num_transaction + 1
            transaction = []
            for item in row:
                # add tung phan tu
                transaction.append(item)
            # complete add transac
            transactions.append(transaction)

    result = []
    frequent_itemsets = []
    support_counts = []

    # tim frequent itemsets
    minSupportCount = options.minsup

    for itemset, support in find_frequent_itemsets(transactions, minSupportCount):
        result.append((itemset, support))
        frequent_itemsets.append(itemset)
        support_counts.append(support)

    # sort ket qua lai cho de xem
    result = sorted(result, key=lambda x: x[0])

    # xuat ket qua
    for itemset, support in result:
        print str(itemset) + ' : ' + str(support)
    
    print "Association rules:"
    threshold = options.threshold
    for frequent_itemset in frequent_itemsets:
        # Tim cac tap con cua frequent_itemset dang xet

        output = sum([map(list, combinations(frequent_itemset, i))
                      for i in range(len(frequent_itemset) + 1)], [])
        # Neu frequent_itemset chi co 2 tap con
        if (len(output) == 4):
                # Kiem tra xem output[1] va frequent_itemset co trong
                # frequent_itemts khong?
            if output[1] in frequent_itemsets:
                frequent_itemset_count = support_counts[
                    frequent_itemsets.index(frequent_itemset)]
                output1_count = support_counts[
                    frequent_itemsets.index(output[1])]
                confidence = frequent_itemset_count / output1_count
                # print str(frequent_itemset) + "** " + str(frequent_itemset_count)
                # print str(output[1]) + "** " + str(output1_count)

                s = frequent_itemset_count / num_transaction
                if confidence >= threshold:
                    print("s : " + str(s) + " - " + "c : " + str("%.2f" % confidence) + " \t " + str(output[1]) + "->" + str(output[2]))
            
            if output[2] in frequent_itemsets:
                frequent_itemset_count = support_counts[
                    frequent_itemsets.index(frequent_itemset)]
                output2_count = support_counts[
                    frequent_itemsets.index(output[2])]
                confidence = frequent_itemset_count / output2_count
                # print str(frequent_itemset) + "** " + str(frequent_itemset_count)
                # print str(output[1]) + "** " + str(output1_count)

                s = frequent_itemset_count / num_transaction
                if confidence >= threshold:
                    print("s : " + str(s) + " - " + "c : " + str("%.2f" % confidence) + " \t " + str(output[2]) + "->" + str(output[1]))

        elif len(output) >= 5:
            for i in range(1, len(output) ):
                if output[i] in frequent_itemsets:  # kiem tra co ton tai hay khong
                    frequent_itemset_count = support_counts[
                        frequent_itemsets.index(frequent_itemset)]
                    outputi_count = support_counts[
                        frequent_itemsets.index(output[i])]
                    confidence = frequent_itemset_count / outputi_count

                    #print str(frequent_itemset) + "** " + str(frequent_itemset_count)
                    #print str(output[i]) + "** " + str(outputi_count)
                    s = frequent_itemset_count / num_transaction
                    if confidence >= threshold:
                        if len(output) - i - 1 != 0:
                            print("s : " + str(s) + " - " + "c : " + str("%.2f" % confidence) + " \t " + str(output[i]) + "->" +
                              str(output[len(output) - i - 1]))

    stop = timeit.default_timer()
    # in thoi gian thuc thi
    print "Execute time:" + str(stop - start)

    # xuat ket qua ra file
    resultfile = open('result.txt', 'w')
    for itemset, support in result:
        resultfile.write(str(itemset) + "\n")
