## How to run
```
python fp.py -s <minimum_supportcount> -t <threshold> <yourfilename>.csv
```
For example: python fp.py -s 2 -t 0.6 test.csv <br>
Corresponding to: 
1. minsupport_count = 2.
2. threshold = 0.6 ( to generate association rules, calculate confidence and support count).
3. run file test.csv

## What's more
* Improve performance in building frequent patterns by running multi-threads.
* Add arguments to run the script in command lines.
* Generate association rules.
* Generate xml file to illustate the FP-tree.

## Reference
1. <a href='https://github.com/enaeseth/python-fp-growth'> Source code from MIT.</a>
2. <a href='https://www-users.cs.umn.edu/~kumar/dmbook/dmslides/chap6_basic_association_analysis.pdf'> How to calculate s and c in association rules.</a>